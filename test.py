import numpy as np
import timeit
import matplotlib.pyplot as plt
import os
import sys
import ctypes

os.environ["MKL_VERBOSE"]="1"
print(np.__config__.show())
mkl_rt = ctypes.CDLL('libmkl_rt.so')
cores=int(sys.argv[1])
mkl_rt.mkl_set_num_threads(ctypes.byref(ctypes.c_int(cores)))
print("Threads ", mkl_rt.mkl_get_max_threads())


m=[200, 400, 800, 1600, 3200]#, 6400, 12800, 25600, 51200]
n=[200, 400, 800, 1600, 3200]#, 6400, 12800, 25600, 51200]
time=[]
for i in range(0, len(m)):
    A_mat=np.random.rand(m[i],n[i])
    B_mat=np.random.rand(m[i],n[i])
    C_mat=np.zeros((m[i],n[i]), dtype=np.int)
    start_time=timeit.default_timer()
    C_mat=np.matmul(A_mat, B_mat)
    stop_time=timeit.default_timer()
    time.append(stop_time-start_time)
    print("Elased time is: ", stop_time-start_time, " s")



#MxM=np.sqrt(np.array(m))
plt.plot(m, time, '-ro')
plt.xlabel('Matrix Size')
plt.ylabel('Execution Time for Threaded (s)')
plt.savefig('Perf' + str(cores) + '.png')
plt.show()

