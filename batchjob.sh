#!/bin/bash

#SBATCH -N 1
#SBATCH -n 1
#SBATCH --cpus-per-task=16
#SBATCH -p test
#SBATCH --mem=32g
#SBATCH -t 00:10:00
#SBATCH -o Matmul_%j.out
#SBATCH -e Matmul_%j.err

#export MKL_NUM_THREADS=1

module load numpy/intel_1.15.1

python test.py
